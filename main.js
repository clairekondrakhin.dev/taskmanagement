//définition du tableau

let tasks = [
    { title: 'Faire les courses', isComplete: false },
    { title: 'Nettoyer la maison', isComplete: true },
    { title: 'Planter le jardin', isComplete: false }
  ];

// Ajouter une tâche : création d'un fonction addTask (fonction fléchée et spread)
/**
 * entrée(s) : Liste de tâches + nouvelle tâche (OrignTaskList, newtask)
 * type en sortie : objet => tableau de tâches complète (newtasklist)
 * quel est le nom de ma fonction : ajouter une tâche à la liste (addtask)
*/



const addTask = (OrignTaskList, newtask) => 
{ let newtasklist = [...OrignTaskList, newtask]; // je reprend le tableau de tâches d'origine et je lui rajoute la nouvelle tâche pour créer la nouvelle liste de tâche
  return newtasklist
}
const newTask1 = { title: 'Peindre la chambre', isComplete: false } // je crée une variable avec la nouvelle tâche
const alltasks = addTask (tasks, newTask1); // Je crée une variable toute les tâches qui execute la fonction addTask avec les arguments task (liste d'origine) et newtask (la nouvelle tâche)

console.log(alltasks); //le résultat du console.log me donne bien un tableau avec la nouvelle tâche 'peindre la chambre

// Effacer une tâche: Définir une fonction removeTask, en utilisant une fonction fléchée et array.filter().
/**
 * entrée(s) : Liste de tâches complète (alltasks))
 * type en sortie : objet => tableau de tâches complète (updatedTasks)
 * quel est le nom de ma fonction : supprimer une tâche à la liste (removeTask)
*/

const removeTask = (alltasks) =>{
  return alltasks.filter(alltask => alltask.isComplete===false); //je demande à la fonction de me retourner la variable alltasks filtré sur 'isComplete' est false pour avoir les taches non exécutées uniquement
}

const updatedTasks = removeTask(alltasks); // je crée une nouvelle variable qui représente la nouvelle liste de tâche qui éxécute la fonction removeTask sur la liste alltasks

console.log (updatedTasks);

// Basculer l'état d'une tâche : Créez une fonction toggleTaskStatus, en incorporant une fonction fléchée, l'opérateur spread et l'opérateur ternaire
/*
Retournez une nouvelle tâche contenant toutes ses informations, et dont le statut "terminé" de la tâche a été inversé (true -> false, false -> true).

Vous avez la possibilité d'utiliser le spread operator (mais ce n'est pas obligatoire).
*/

const toggleTaskStatus = (updatedTasks)=> {
  updatedTasks.isComplete === false ? 'true':'true';}

const reUpdatedTask = toggleTaskStatus(updatedTasks);
console.log(reUpdatedTask);

